import './Cell.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';

const cellStyle = (size, isBlack) => ({
	height: `${100 / size}%`,
	width: `${100 / size}%`,
	backgroundColor: isBlack == true ? 'black' : 'white',
	color: isBlack == true ? 'white' : 'black'
});

function Cell(props) {
	return (
		<div className="Cell" style={cellStyle(props.size, props.isBlack)}>
			<div className="wrapper">
				<FontAwesomeIcon icon={faCoffee} />
				Text
			</div>
		</div>
	);
}

export { Cell };
