import './Chessboard.css';
import { Cell } from './../Cell';

function Chessboard() {
	let size = 5;

	let generateGrid = (size) => Array.from({ length: size ** 2 }, (_, i) => <Cell size={size} isBlack={!(i % 2)} />);

	return <div className="Chessboard">{generateGrid(size)}</div>;
}

export { Chessboard };
