import './App.css';
import { Chessboard } from './../components/Chessboard';

function App() {
	return (
		<div className="App">
			<div className="centered">
				<Chessboard />
			</div>
		</div>
	);
}

export { App };
